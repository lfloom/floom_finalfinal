﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Points : MonoBehaviour
{
    public Text sugarText;
    public Text lemonText;
    public Text waterText;
    public Text loveText;

    public Text winText;
    // public Text gameover;
    public Text Gameover;


    private static int sugarCount =0;
    private static int lemonCount = 0;
    private static int waterCount=0;
    private static int loveCount =0;

    //variables for random number of ingredients needed to win
    
    private int sugarRand;
    private int lemonRand;
    private int waterRand;
    private int loveRand;
    


    //private int counter;
    // Start is called before the first frame update
    void Start()
    {
        //used for score
        //counter = 0;

        //make score go up
        /*
        sugarCount = 0;
        lemonCount = 0;
        waterCount = 0;
        loveCount = 0;
        */

        //setting random variables for ingredients

        sugarRand = 10;
        lemonRand = 7;
        waterRand = 16;
        loveRand = 11;
        

        //setting original scores 
        SugarScore();
        LemonScore();
        WaterScore();
        LoveScore();

        winText.enabled = false;
        Gameover.enabled = false;
       // gameover.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        won();
        
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if(other.gameObject.CompareTag("duck") && gameObject.CompareTag("realSugar"))
        {
            gameObject.SetActive(false);
            sugarCount++;
            SugarScore(); 
        }
        else if (other.gameObject.CompareTag("duck") && gameObject.CompareTag("realLemon"))
        {
            gameObject.SetActive(false);
            lemonCount++;
            LemonScore();
        }
        else if (other.gameObject.CompareTag("duck") && gameObject.CompareTag("realWater"))
        {
            gameObject.SetActive(false);
            waterCount++;
            WaterScore();
        }
        else if (other.gameObject.CompareTag("duck") && gameObject.CompareTag("realLove"))
        {
            gameObject.SetActive(false);
            loveCount++;
            LoveScore();
        }
        
        else if(other.gameObject.CompareTag("duck"))
        {
     
            gameObject.SetActive(false);
            other.gameObject.SetActive(false);
            //lose(); 
            Gameover.enabled = true; 
        

            //score goes down 
        }

        else if(other.gameObject.CompareTag("bubble"))
            {
                gameObject.SetActive(false);
            }
    }


    void SugarScore()
    {
        //GameObject sugarObject = GameObject.Find("SugarStat");
        //Text sugarText = sugarObject.GetComponent<Text>();
        sugarText.text = "Sugar: " + sugarCount.ToString() + "/" + sugarRand;
        
    }
    void LemonScore()
    {
        lemonText.text = "Lemons: " + lemonCount.ToString() + "/" + lemonRand;

    }
    void WaterScore()
    {
        waterText.text = "Water: " + waterCount.ToString() + "/" + waterRand;

    }
    void LoveScore()
    {
        loveText.text = "Love: " + loveCount.ToString() + "/" + loveRand;

    }

    void won()
    {
        if((sugarCount == sugarRand) && (lemonCount == lemonRand) && (waterCount == waterRand) && (loveCount == loveRand))
        {
            winText.enabled = true; 
        }
    }


}
