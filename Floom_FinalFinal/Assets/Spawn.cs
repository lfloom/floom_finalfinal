﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawn : MonoBehaviour
{
    
    //variables 
    public GameObject obj;
 

    // Start is called before the first frame update
    void Start()
    {
        //spawning evil spirits on game start 
        int i = 0;
        while (i < 5)
        {
            SpawnOnElement(); 
            //SpawnRandomObject();
            i++; 
        }
    }

    // Update is called once per frame
    void Update()
    {

    }

    void SpawnOnElement()
    {
        Instantiate(obj, transform.position + new Vector3(Random.Range(-1.4f, 1.4f), Random.Range(-1.4f, 1.4f), 0), Quaternion.identity);
    }
    /*
    void SpawnRandomObject()
    {
        Instantiate(obj, new Vector3(Random.Range(-7f, -6f), Random.Range(2.5f, 4f), -9.25f), Quaternion.identity);
    }
    */
}
