﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item : MonoBehaviour
{
    public GameObject sugar;
    public GameObject lemon;
    public GameObject water;
    public GameObject love;

    //falling objects timer
    public float maxTime = 5;
    public float minTime = 2;

    //time currently stored
    private float time;

    //object spawned at this time
    private float spawnTime;


    void Start()
    {
        SetRandomTime();
        time = minTime;
    }

    // Update is called once per frame
    void Update()
    {

    }

    void FixedUpdate()
    {

        //Counts up
        time += Time.deltaTime;

        //Check if its the right time to spawn the object
        if (time >= spawnTime)
        {
            SpawnObject();
            SetRandomTime();
        }

    }

    //Spawns the object and resets the time
    void SpawnObject()
    {
        time = minTime;
        int i = Random.Range(1, 5);
        GameObject obj;

        if (i == 1)
        {
            obj = sugar;
        }
        else if (i == 2)
        {
            obj = lemon;
        }
        else if (i == 3)
        {
            obj = water;
        }
        else
        {
            obj = love;
        }


        Instantiate(obj, transform.position, Quaternion.identity);
    }

    //Sets the random time between minTime and maxTime
    void SetRandomTime()
    {
        spawnTime = Random.Range(minTime, maxTime);
    }

    void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            other.gameObject.SetActive(false);

            //Then Display Loser Text 
        }
    }
}

