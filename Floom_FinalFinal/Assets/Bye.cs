﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Bye : MonoBehaviour
{

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnCollisionEnter2D(Collision2D other)
    {
        
            if (other.gameObject.CompareTag("evil"))
            {
                other.gameObject.SetActive(false);
            }

        if (other.gameObject.CompareTag("earth"))
        {
            gameObject.SetActive(false);
        }

    }
}
