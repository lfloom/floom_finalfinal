﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Grapes : MonoBehaviour
{
    public GameObject grape;
    public Text healthText;
    public Text gameOver;
    private static int healthCount =20;

    //falling objects timer
    public float maxTime = 5;
    public float minTime = 2;

    //time currently stored
    private float time;

    //object spawned at this time
    private float spawnTime;


    void Start()
    {
        SetRandomTime();
        time = minTime;

        //healthCount = 20;
        healthScore();

        //game over text not showing
        gameOver.enabled = false; 
    }

    // Update is called once per frame
    void Update()
    {

    }

    void FixedUpdate()
    {

        //Counts up
        time += Time.deltaTime;

        //Check if its the right time to spawn the object
        if (time >= spawnTime)
        {
            SpawnObject();
            SetRandomTime();
        }

    }

    //Spawns the object and resets the time
    void SpawnObject()
    {
        Instantiate(grape, transform.position, Quaternion.identity);
    }

    //Sets the random time between minTime and maxTime
    void SetRandomTime()
    {
        spawnTime = Random.Range(minTime, maxTime);
    }
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("duck"))
        {
            gameObject.SetActive(false);
            healthCount--;
            healthScore();
            gameOver.enabled = true; 

        }
    }

    void healthScore()
    {
        healthText.text = "HEALTH POINTS: " + healthCount.ToString();
        if(healthCount == 0)
        {
            gameOver.enabled = true;
        }

    }
}
    
